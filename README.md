# frontendstore-gridcss
Template for store.
## Description
## BEM Methodology (Block, Element, Modifier).

At the FronEnd Store, we use the BEM (Block, Element, Modifier) ​​methodology to organize our CSS code. BEM is a widely used methodology that allows us to write modular and scalable CSS. With BEM, we divide our styles into blocks, elements, and modifiers, which helps us keep our code uncluttered and maintainable. In addition, we also make use of Grid to create flexible and adaptable layouts for different screen sizes.

## MediaQueries
We make use of Media Queries in CSS to adapt our styles to different screen resolutions. Media Queries allow us to set specific styles for different devices or screen sizes, which helps us create responsive and attractive designs.

## Support
franciscosobrevilla97@gmail.com

## Project status
Suspended
